var pathModule = require('path');
var browserify = require('browserify');
var handlerHelper = require('../handlerHelper');

exports.makeHandler = function (status, pathname) {
    var minifyJS = false;
    var bundleJS = false;
    var sourceMap = false;
    var BROWSERIFY = new Object; // serve as a flag

    
    return {
        addToCtx: function (ctx) {
            ctx.BROWSERIFY = BROWSERIFY;
            ctx.js = function (path, opt) {
                var pathFromRoot = pathModule.join(pathModule.dirname(pathname), path);
                if (opt === BROWSERIFY) {
                    status.addJsBrowserifyPath(pathFromRoot);
                } else {
                    status.addJsPath(pathFromRoot);
                }
            };
            ctx.minifyJS = function (flag) {
                minifyJS = flag
            };
            ctx.bundleJS = function (flag) {
                bundleJS = flag;
            };
            ctx.sourceMap = function (flag) {
                sourceMap = flag;
            };
        },
        
        rewrite: function (window, $, _env, _status, topLevel) {
            if (!topLevel) {
                return;
            }

            function addScript (src) {
                $('body').append($('<script>').attr('src', src));
            }

            for (let path of status.getJsPaths()) {
                addScript(path);
            }

            addScript(handlerHelper.marshal('js', 'browserify', 
                                            status.getJsBrowserifyPaths(),
                                            sourceMap));
            
        }
    };
};

exports.browserify = function browserify_ (paths, sourceMapSwitch, res) {
    res.writeHead('200', {'content-type': 'application/javascript'});
    //fixme: missing file handling
    var b = browserify(paths, {debug: sourceMapSwitch});
    b.bundle().pipe(res);

};
