var fs = require('fs');
var ejs; //lazy load

exports.makeHandler = function () {
    var minifyHTML = false;
    var translateTable = null;

    return {
        addToCtx: function (ctx) {
            ctx.minifyHTML = function (flag) {
                minifyHTML = flag;
            };

            ctx.translate = function (tableFilePath) {
                translateTable = JSON.parse(fs.readFileSync(tableFilePath));
            };
        },

        rewrite: function (window, $, env) {
            // expand templates
            $('script[type="text/x-ejs-template"]').each(function () {
                ejs = ejs || require('ejs');
                var out = ejs.render($(this).html(), {attrs: env.attrs});
                $(this).replaceWith(out);
            });

            // translate
            if (!!translateTable) {
                var walk = window.document.createTreeWalker(
                    window.document,
                    window.NodeFilter.SHOW_TEXT,
                    null,
                    false);
                var n;
                if (!env.flag.match(/dont_translate/i)) {
                    while (n=walk.nextNode()) {
                        var t = n.nodeValue.trim();
                        if (!t) continue;
                        var row = translateTable.find(r => r[0]===t);
                        if (!!row && row[1]) {
                            n.nodeValue = row[1];
                        }
                    }
                }
                /*
                $(window.document.body)
                    .contents()
                    .filter(function () {
                        return this.nodeType === window.Node.TEXT_NODE;
                    }).each(function (i, elem) {
                        var t = elem.nodeValue.trim();
                        if (!t) return;
                        var translation = translateTable.find(row => row[0]===t);
                        if (!!translation) {
                            console.log('%s->%s', elem.nodeValue, translation);
                            elem.nodeValue = translation;
                        }
                    });
                */
            }
        }
    };
};
