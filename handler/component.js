var path = require('path');
var util = require('util');
var handlerHelper = require('../handlerHelper');

/*
automatics emits css & js
emits html where declared
*/
exports.makeHandler = function (status) {
    var componentDecls = [];

    return {
        addToCtx: function (ctx) {
            ctx.component = function (path) {
                componentDecls.push({path: path});
            };
        },

        rewrite: async function (window, $, env, status) {
            var components = {};
            for (let decl of componentDecls) {
                var name = decl.name;
                var component = {name: name};

                if (!name) {
                    name = path.basename(decl.path, path.extname(decl.path));
                }
                
                component.path = decl.path;
                components[name] = component;
            };

            // <componentHtml>
            for (let n of $('componentHtml')) {
                var componentName = $(n).attr('name');

                var attrs = {}
                for (var i=0; i<n.attributes.length; i++) {
                    var a = n.attributes[i];
                    attrs[a.name] = a.value;
                }

                var component = components[componentName];
                if (!component) {
                    throw new Error('Component '+componentName+' not found');
                }

                //todo: error handling
                console.log('loading component from', component.path);
                //todo: propagate up error
                var html = await handlerHelper.rewrite(component.path, Object.assign({}, env, {attrs: attrs}), false, status);

                $(n).replaceWith(html);
            }
        }
    };
};

