var pathModule = require('path');

exports.makeHandler = function (status, pathname) {
    var minifyCSS = false;
    var embedCSS = false;
    var basePath = '';

    return {
        addToCtx: function (ctx) {
            ctx.css = function (path, opt) {
                var pathFromRoot = pathModule.join(pathModule.dirname(pathname), path);

                status.addCssPath({path: pathFromRoot, opt: opt});
            };
            ctx.embedCSS = function (flag) {
                embedCSS = flag;
            };
            ctx.minifyCSS = function (flag) {
                minifyCSS = flag;
            };
            ctx.setCSSBase = function (path) {
                basePath = path;
            };
        },

        rewrite: function (window, $, _env, _status, topLevel) {
            if (!topLevel) {
                return;
            }

            function addStyle (path) {
                $('head').append($('<link rel="stylesheet" type="text/css">').attr('href', path));
            }

            for (let path of status.getCssPaths()) {
                addStyle(pathModule.join(basePath, path.path));
            };
        }
    };
};
