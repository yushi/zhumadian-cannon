var querystring = require('querystring');
var url = require('url');
var fs = require('fs');
var path = require('path');
var util = require('util');
var vm = require('vm');
var jsdom = require('jsdom');

// class Status
makeStatus = function () {
    var cssPaths = [];
    var jsPaths = [];
    var browserifyPaths = [];

    return {
        //todo: dedup
        addJsBrowserifyPath: function (path) {
            console.log('adding js browserify', path);
            browserifyPaths.push(path);
        },

        getJsBrowserifyPaths: function () {
            return browserifyPaths;
        },
        
        addCssPath: function (path) {
            cssPaths.push(path);
        },

        getCssPaths: function (path) {
            return cssPaths;
        },

        addJsPath: function (path) {
            console.log('adding js', path);
            jsPaths.push(path);
        },

        getJsPaths: function () {
            return jsPaths;
        }
    }
};

exports.marshal = function marshal (handler, func, ...params) {
    return `/__${handler}_${func}?`+ querystring.stringify(params);
};

exports.handle = function (req, res) {
    var m = req.url.match(/^\/__(\w+)_(\w+)(\?|$)/);
    if (!m) {
        return false;
    }
    var handler = m[1];
    var func = m[2];

    var handlerFunc = HANDLER_MODULES.find(m=>m.name===handler).module[func];

    var query = url.parse(req.url, true).query;
    var params = Object.keys(query).map(Number).sort((a,b)=>a-b).map(String)
        .map(function (k) {
            var v = query[k];
            if (v === 'true') return true;
            if (v === 'false') return false;
            return v;
        });
    handlerFunc(...params, res);
    return true;
};


var HANDLER_MODULES = ['component'/*must be first*/, 'js', 'css', 'html', 'watch'].map(function (name) {
    return {name: name, module: require('./handler/'+name)};
});

exports.rewrite = async function rewrite (pathname, env, topLevel, status=makeStatus()) {
    var rawHtml = fs.readFileSync(pathname, 'utf8');

    var window = await util.promisify(jsdom.env)(
        pathname, 
        [path.join(__dirname, 'node_modules/jquery/dist/jquery.js')]);

    var $ = window.$;
    var zmdcScriptNode = $('head script[type=ZhumadianScript]');
    if (!zmdcScriptNode.length) {
        return rawHtml;
    }

    var handlers = HANDLER_MODULES.map(function (m) {
        return m.module.makeHandler(status, pathname);
    });
    var ctx = makeContext(handlers, env);

    var code = zmdcScriptNode.text();

    try {
        vm.runInNewContext(
            code,
            ctx,
            {filename: pathname,
             lineOffset: getLineNumber(code, rawHtml) // ugly
            });
    } catch (e) {
        return makeErrorPage(e);
    }

    //todo: error handling
    for (let handler of handlers) {
        await handler.rewrite(window, $, env, status, topLevel)
    }

    // remove what jsdom inserts
    window.$('script.jsdom').remove();
    zmdcScriptNode.remove()

    return jsdom.serializeDocument(window.document);
};

function makeContext (handlers, env) {
    var ctx = {};
    handlers.forEach(function (handler) {
        handler.addToCtx(ctx);
    });
    ctx.env = env.env;
    return ctx;
}

function getLineNumber (code, raw) {
    var offset = raw.indexOf(code);
    if (offset === -1) {
        return NaN;
    }
    return raw.substr(0, offset+1).match(new RegExp(/\n/, 'gm')).length-1;
}

function makeErrorPage (e) {
    return e.stack;
}
