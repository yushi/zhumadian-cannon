var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');
var handlerHelper = require('./handlerHelper');

var PORT = process.argv[2];
var DEFAULT_ENV = process.argv[3] || '';

function fileExists (pathname) {
    return fs.existsSync(pathname);
}

http.createServer(async function (req, res) {
    console.log('handling request:', req.url);
    var urlObj = url.parse(req.url);
    var pathname = urlObj.pathname.substr(1) || 'index.html';

    var p1 = pathname.split('/')[0];
    if (p1.match(/\.html$/)) {
        pathname = p1;
    }

    var env = {env: DEFAULT_ENV, flag: req.headers.flag || ''};

    if (handlerHelper.handle(req, res)) {
        return;
    }
    
    if (!fileExists(pathname)) {
        res.writeHead(404, {'content-length': '0'});
        res.end();
        return;
    }

    if (['.html', '.htm'].indexOf(path.extname(pathname)) !== -1) {

        var out = await handlerHelper.rewrite(pathname, env, true);

        if (out) {
            res.setHeader('Content-type', 'text/html; charset=utf-8');
            res.end(out);
            return;
        }
    }

    //fixme: content-type
    fs.createReadStream(pathname).pipe(res);

}).listen(PORT);


